
//Preferences

$RedEval_OutputFile = "config/RedEval.out";
$RedEval_MaxLinesShown = 100;

//Shortcut Functions

function fc(%name){ return findClientByName(%name); }
function fp(%name){ return findClientByName(%name).player; }
function fcn(%name){ return findClientByName(%name); }
function fpn(%name){ return findClientByName(%name).player; }
function fcbn(%name){ return findClientByName(%name); }
function fpbn(%name){ return findClientByName(%name).player; }
function td(){ transmitDatablocks(); }

//Permissions

function RedEval_ClientCanEval(%client){
	return %client.isLocal || %client.getBlid()==getNumKeyId() || %client.canEval;
}

//Logger

if(!isObject(RedEval_ConsoleLogger)){
	new ConsoleLogger(RedEval_ConsoleLogger, $RedEval_OutputFile);
	RedEval_ConsoleLogger.level = 0;
	RedEval_ConsoleLogger.detach();
}

//Evaluating

luaeval("function isvalidcode(code) return loadstring(code)~=nil end");

function RedEval_Evaluate(%client, %code){
	
	//Prepare local vars
	
	%c = %cl = %client;
	%p = %pl = %player = %client.player;
	%b = %bg = %brickGroup = %client.brickGroup;
	%m = %mg = %miniGame = %client.miniGame;
	
	if(isObject(%player)){
		%data   = %d = %pl.getDatablock();
		%pos    = %pl.getPosition();
		%vel    = %pl.getVelocity();
		%rot    = getWords(%pl.getTransform(), 3, 6);
		%trans  = %pl.getTransform();
		
		%box    = %pl.getWorldBox();
		%size   = vectorSub(getWords(%box, 3, 5), getWords(%box, 0, 2));
		%scale  = %pl.getScale();
		
		%mount  = %pl.getObjectMount();
		
		%eyeP   = %pl.getEyePoint();
		%eyeV   = %pl.getEyeVector();
		
		%ray    = containerRayCast(%eyeP, vectorAdd(%eyeP, vectorScale(%eyeV, 1000)), $TypeMasks::All, %pl, $eexc);
		
		if(isObject(%hit = firstWord(%ray))){
			%hitP  = getWords(%ray, 1, 3);
			%hitN  = getWords(%ray, 4, 6);
			%hitR  = getWords(%hit.getTransform(), 3, 6);
			%hitB  = %hit.getWorldBox();
			%hitBS = vectorSub(getWords(%hitB, 3, 5), getWords(%hitB, 0, 2));
			
			if(%hit.getType() & $TypeMasks::PlayerObjectType){
				%hitC = %hit.client;
			}
		}
	}
	
	//Process shortcuts
	
	%code = trim(%code);
	
	%lua = false;
	if(getSubStr(%code, 0, 1)$="\'") {
		%lua = true;
		%code = getSubStr(%code, 1, strLen(%code));
	}
	
	%firstchar = getSubStr(%code, 0, 1);
	%rest = getSubStr(%code, 1, strLen(%code)-1);
	if(%firstchar$="!"){
		%code = %rest @ "()";
	}
	else if(%firstchar$="^"){
		%code = (%lua ? "lua" : "") @ "exec(\"" @ %rest @ "\");";
	}
	
	%lastchar = getSubStr(%code, strLen(%code)-1, 1);
	if(%lastchar!$=";" && %lastchar!$="}" && !%lua){
		%code = "echo(" @ %code @ ");";
	}
	
	//Evaluate
	
	if(isFile($RedEval_OutputFile)){ fileDelete($RedEval_OutputFile); }
	
	echo("CHAT EVAL" @ (%lua ? " (LUA)" : "") @ ": " @ %client.name @ ": \"" @ %code @ "\"");
	
	RedEval_ConsoleLogger.attach();
	
	if(%lua) {
		%pcode = "print(" @ %code @ ")";
		if(luacall("isvalidcode", %pcode)) {
			luaeval(%pcode);
		} else {
			luaeval(%code);
		}
	} else {
		eval(%code);
	}
	
	RedEval_ConsoleLogger.detach();
	
	//Output
	
	%file = new FileObject();
	%file.openForRead($RedEval_OutputFile);
	
	%pad = "    ";
	
	%linesShown = 0;
	%linesTotal = 0;
	while(!%file.isEof()){
		%line = %file.readLine();
		
		if(
			trim(%line)!$="" &&
			getSubStr(%line, 0, 11)!$="BackTrace: "
		){
			if(%linesShown<$RedEval_MaxLinesShown){
				%line = strReplace(%line, "\c0", "");
				%line = strReplace(%line, "\c1", "");
				%line = strReplace(%line, "\c2", "");
				%line = strReplace(%line, "\c3", "");
				%line = strReplace(%line, "\c4", "");
				%line = strReplace(%line, "\c5", "");
				%line = strReplace(%line, "\c6", "");
				%line = strReplace(%line, "\c7", "");
				%line = strReplace(%line, "\c8", "");
				%line = strReplace(%line, "\c9", "");
				messageAll('', '<color:999999>%1> %2', %pad, %line);
				%linesShown++;
			}
			%linesTotal++;
		}
	}
	
	if(%linesShown<%linesTotal){
		messageAll('', '<color:ff6666>%1> \c6~~! (truncated, %2 of %3 lines shown)', %pad, %linesShown, %linesTotal);
	}
	
	%file.close();
	%file.delete();
}

package RedEvalChat{
	function serverCmdMessageSent(%client, %msg){
		if(getSubStr(%msg, 0, 1)$="\\" && RedEval_ClientCanEval(%client)){
			%text = getSubStr(%msg, 1, strLen(%msg)-1);
			
			if(strLen(%text)>0 && getSubStr(%text, strLen(%text)-1, 1)$="\\"){ //Multi-Line Eval
				%text = getSubStr(%text, 0, strLen(%text)-1);
				
				messageAll('', '\c3%1 <color:999999>(ML) ==> \c6%2', %client.name, %text);
				
				%client.RedEval_MlBuffer = %client.RedEval_MlBuffer @ "\n" @ %text;
			}else{ //Single or Last Line
				//Announce
				messageAll('', '\c3%1 <color:999999>==> \c6%2', %client.name, %text);
				
				//Evaluate
				RedEval_Evaluate(%client, %client.RedEval_MlBuffer @ "\n" @ %text);
				
				//Clear Buffer
				%client.RedEval_MlBuffer = "";
			}
		}else{
			Parent::serverCmdMessageSent(%client, %msg);
		}
	}
};
activatePackage(redEvalChat);

function rer(){
	exec("./redeval.cs");
}
